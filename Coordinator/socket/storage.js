const admin = require('firebase-admin');
const { Server } = require('ws');
const serviceAccount = require('./connectedjacket3.json');


admin.initializeApp({
  credential: admin.credential.cert(serviceAccount)
});

const db = admin.firestore();


module.exports.registerSensors = async function (sample) {
var val;

  const docRef = db.collection('sensors').doc('light');
  if (sample >= 500){
    val = 0;
  }
  else {
    val =1;
    //console.log("registerSensor");
    //console.log(sample);
  }
 

  const sensor = {
    value: sample,
    activated: val,
  }

  docRef.set(sensor);

}

module.exports.registerBlinkerR = async function (sample) {

  const docRef = db.collection('blinker').doc('right');

  const sensor = {
    value: sample,
  }

  docRef.set(sensor);
}

module.exports.registerBlinkerL = async function (sample) {

  const docRef = db.collection('blinker').doc('left');

  //console.log("register");
  //console.log(sample);

  const sensor = {
    value: sample,
  }

  docRef.set(sensor);
}

module.exports.listSensors = async function () {
  var document2;
  var document;

  const docRef = db.collection('sensors').doc('light');
  document = await docRef.get();
  document2 = document.data().value.AD3;// recup champ value de valeur dans light
  exports.document2 = document2;
}

module.exports.listActivated = async function () {
  var document5;
  var document;

  const docRef = db.collection('sensors').doc('light');
  document = await docRef.get();
  document5 = document.data().activated;// recup champ value de valeur dans light
  exports.document5 = document5;
}

module.exports.listPermanent = async function () {
  var document6;
  var document;

  const docRef = db.collection('phone').doc('light');
  document = await docRef.get();
  document6 = document.data().permanant;// recup champ value de valeur dans light
  
  //console.log(document6);
  exports.document6 = document6;
}


module.exports.listBlinkerR = async function () {
  var document3;
  var document;


  const docRef = db.collection('blinker').doc('right');
  document = await docRef.get();
  document3 = document.data().value;// recup champ value de valeur dans light
  
  console.log("List R");
  console.log(document3);
  
  exports.document3 = document3;

}

module.exports.listBlinkerL = async function () {
  var document4;
  var document;


  const docRef = db.collection('blinker').doc('left');
  document = await docRef.get();
  document4 = document.data().value;// recup champ value de valeur dans light
  
  console.log("List L");
  console.log(document4);
  exports.document4 = document4;

}


