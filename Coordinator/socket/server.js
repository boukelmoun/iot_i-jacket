var SerialPort = require('serialport');
var xbee_api = require('xbee-api');
var C = xbee_api.constants;
var frame_obj;
var storage = require("./storage");
var start;
var end;
const BROADCAST = "FFFFFFFFFFFFFFFF";

require('dotenv').config()

const SERIAL_PORT = process.env.SERIAL_PORT;

var xbeeAPI = new xbee_api.XBeeAPI({
  api_mode: 2
});

let serialport = new SerialPort(SERIAL_PORT, {
  baudRate: 9600,
}, function (err) {
  if (err) {
    return console.log('Error: ', err.message);
  }
});

function blink(command, commandParameter){
  frame_obj = {//Led D0 On
    type: C.FRAME_TYPE.REMOTE_AT_COMMAND_REQUEST,
    destination64: BROADCAST,
    command: command,
    commandParameter: [commandParameter],
  };
  xbeeAPI.builder.write(frame_obj);
}

serialport.pipe(xbeeAPI.parser);
xbeeAPI.builder.pipe(serialport);

serialport.on("open", function () {
  var frame_obj = { // AT Request to be sent
    type: C.FRAME_TYPE.AT_COMMAND,
    command: "NI",
    commandParameter: [],
  };

  xbeeAPI.builder.write(frame_obj);

  frame_obj = { // AT Request to be sent
    type: C.FRAME_TYPE.REMOTE_AT_COMMAND_REQUEST,
    destination64: BROADCAST,
    command: "NI",
    commandParameter: [],
  };
  xbeeAPI.builder.write(frame_obj);

  // LED D2 always lit
  blink("D2", 0x05);

});

// All frames parsed by the XBee will be emitted here
xbeeAPI.parser.on("data", async function (frame) {
  //on packet received, dispatch event
  if (C.FRAME_TYPE.ZIGBEE_RECEIVE_PACKET === frame.type) {
    console.log("C.FRAME_TYPE.ZIGBEE_RECEIVE_PACKET");
    let dataReceived = String.fromCharCode.apply(null, frame.data);
    console.log(">> ZIGBEE_RECEIVE_PACKET >", dataReceived);
  }

  if (C.FRAME_TYPE.NODE_IDENTIFICATION === frame.type) {
    console.log("NODE_IDENTIFICATION");

  } else if (C.FRAME_TYPE.ZIGBEE_IO_DATA_SAMPLE_RX === frame.type) {
    start = Date.now();
    storage.listPermanent();
    var permanant = storage.document6;
    
    const millis = start - end;

    if ( millis > 1000){

      if (frame.remote16 == '391d'){
        storage.registerBlinkerR(frame.digitalSamples.DIO0);
        storage.registerBlinkerL(frame.digitalSamples.DIO1);
      }
      else if (permanant == 0) {
        storage.registerSensors(frame.analogSamples.AD3);
        storage.listSensors();
        storage.listActivated();
      }

      storage.listBlinkerR();
      storage.listBlinkerL();

      var light = storage.document2;
      var activated = storage.document5;
      var blinkRight = storage.document3;
      var blinkLeft = storage.document4;

      if ( ( (light <= 500) || (activated ==1) || permanant ==1 ) ) {// if light <= threshold => DIO4 On
        blink("D4", 0x05);
      } else if ( permanant == 0) { // else DIO4 Off
        blink("D4", 0x04);
      }

      if (blinkRight == 1){//Led D0(R) On and D1(G) Off
        blink("D0", 0x05);
        blinkLeft = storage.document4;
      }
      else if (blinkRight == 0) {//Led D0 Off if blinkRight off
        blink("D0", 0x04);
      }

      if (blinkLeft == 1) {//Led D1(R) On & D0(G) Off
        blink("D1", 0x05);
        blinkRight = storage.document3;
      }
      else if (blinkLeft == 0) {//Led D1 Off if blinkLeft off
        blink("D1", 0x04);
      }

    }
    end = Date.now();

  } else if (C.FRAME_TYPE.REMOTE_COMMAND_RESPONSE === frame.type) {
    //console.log("REMOTE_COMMAND_RESPONSE");
  } else {
    console.debug(frame);
    let dataReceived = String.fromCharCode.apply(null, frame.commandData);
    console.log(dataReceived);
  }

});
