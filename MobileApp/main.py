import os
from flask import Flask, request, jsonify
from firebase_admin import credentials, firestore, initialize_app
import socket
import firebase_admin
from firebase_admin import credentials



app = Flask(__name__)
 
cred = credentials.Certificate("connectedJacket.json")
firebase_admin.initialize_app(cred)
db = firestore.client()
blinker = db.collection('blinker')
light = db.collection('sensors')
permanant = db.collection('phone')

@app.route("/")
def showHomePage():
    return "bonsouar"
    
@app.route('/updateRight')
def updateRight():
    info = blinker.document(u'right').get()
    info = info.to_dict()
    if (info["value"] == 0):
        blinker.document(u'right').set({
            u'value' : 1
        })
        blinker.document(u'left').set({
            u'value' : 0
        })
    else:
        blinker.document(u'right').set({
            u'value' : 0
        })
    return "Changer clignotant droit"
    
@app.route('/updateLeft')
def updateLeft():
    info = blinker.document(u'left').get().to_dict()
    if (info["value"] == 0):
        blinker.document(u'left').set({
            u'value' : 1
        })
        blinker.document(u'right').set({
            u'value' : 0
        })
    else:
        blinker.document(u'left').set({
            u'value' : 0
        })
    return "Changer clignotant gauche"
    
    

@app.route('/updateLight')
def updateLight():
    info = permanant.document(u'light').get()
    info = info.to_dict()
    if (info["permanant"] == 0):
        permanant.document(u'light').set({
            u'permanant' : 1
        })
    else:

        permanant.document(u'light').set({
            u'permanant' : 0
        })
        
    return "phare allumé"
  
if __name__ == "__main__":
  app.run(host="192.168.56.1")
