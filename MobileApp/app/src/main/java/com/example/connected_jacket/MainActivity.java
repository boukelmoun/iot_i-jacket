package com.example.connected_jacket;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.FileInputStream;
import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity {

    Button buttonL;
    Button buttonR;
    Button buttonOnOff;
    boolean leftIsLit = false;
    boolean rightIsLit = false;
    boolean onOffIsLit = false;
    Request request;

    TextView debug;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonL = findViewById(R.id.buttonLeft);
        buttonR = findViewById(R.id.buttonRight);
        buttonOnOff = findViewById(R.id.buttonOnOff);
        buttonL.setTextColor(Color.WHITE);
        buttonR.setTextColor(Color.WHITE);
        buttonOnOff.setTextColor(Color.WHITE);
        buttonL.setBackgroundColor(Color.BLUE);
        buttonR.setBackgroundColor(Color.BLUE);
        buttonOnOff.setBackgroundColor(Color.BLUE);


        OkHttpClient okHttpClient= new OkHttpClient();



        buttonL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Do something if left is lit, and something else if it isn't

                request = new Request.Builder().url("http://192.168.56.1:5000/updateLeft").build();
                if (leftIsLit){
                    buttonL.setBackgroundColor(Color.BLUE);
                } else {
                    buttonL.setBackgroundColor(Color.RED);
                }
                if (rightIsLit){
                    buttonR.setBackgroundColor(Color.BLUE);
                    rightIsLit = !rightIsLit;
                }
                leftIsLit = !leftIsLit;
                //leftListener(v);
                okHttpClient.newCall(request).enqueue(new Callback() {
                    @Override
                    public void onFailure(@NonNull Call call, @NonNull IOException e) {
                        //Toast.makeText(MainActivity.this, "network not found", Toast.LENGTH_LONG).show();
                        System.out.println("ça marche pas ***********------------------------------------------------");
                        //e.printStackTrace();
                    }

                    @Override
                    public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {

                    }
                });
                /*finish();
                startActivity(getIntent());*/
            }
        });
        buttonR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                request = new Request.Builder().url("http://192.168.56.1:5000/updateRight").build();
                System.out.println("pass");
                if (rightIsLit){
                    buttonR.setBackgroundColor(Color.BLUE);
                } else {
                    buttonR.setBackgroundColor(Color.RED);
                }
                if (leftIsLit){
                    buttonL.setBackgroundColor(Color.BLUE);
                    leftIsLit = !leftIsLit;
                }
                rightIsLit = !rightIsLit;
                //rightListener(v);
                okHttpClient.newCall(request).enqueue(new Callback() {
                    @Override
                    public void onFailure(@NonNull Call call, @NonNull IOException e) {
                        //Toast.makeText(MainActivity.this, "network not found", Toast.LENGTH_LONG).show();
                        System.out.println("ça marche pas ***********------------------------------------------------");
                        //e.printStackTrace();
                    }

                    @Override
                    public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {

                    }
                });
            }
        });
        buttonOnOff.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                request = new Request.Builder().url("http://192.168.56.1:5000/updateLight").build();
                if (onOffIsLit){
                    buttonOnOff.setBackgroundColor(Color.BLUE);
                    buttonOnOff.setText("Off");
                } else {
                    buttonOnOff.setBackgroundColor(Color.RED);
                    buttonOnOff.setText("On");
                }
                onOffIsLit = !onOffIsLit;
                //onOffListener(v);
                okHttpClient.newCall(request).enqueue(new Callback() {
                    @Override
                    public void onFailure(@NonNull Call call, @NonNull IOException e) {
                        //Toast.makeText(MainActivity.this, "network not found", Toast.LENGTH_LONG).show();
                        System.out.println("ça marche pas ***********------------------------------------------------");
                        //e.printStackTrace();
                    }

                    @Override
                    public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {

                    }
                });
            }
        });





    }

}