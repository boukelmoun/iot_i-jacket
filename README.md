# iot_I-Jacket


# Pour lancer l'écoute du coordinateur, aller dans le dossier Coordinateur/socket/ et taper dans une console : 
> yarn start

# Au niveau de l'application:

# Lancer le serveur en tapant les différentes commandes avec une console ouvert dans MobileApp/ :
> pip install -r requirements.txt
> export GOOGLE_APPLICATION_CREDENTIALS=connectedjacket.json
> python main.py

# Lancer l'application dans Android Studio

# En cas de probleme/erreur au niveau de l'adresse de requête, veuillez modifier l'adresse en mettant celui de votre PC dans le main.py et dans le mainActivity du code android.
